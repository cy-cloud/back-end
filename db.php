<?php 

require __DIR__ . '/dbData.php';

try {
    $db = new PDO("mysql:host=$mysql_host;dbname=$mysql_dbname;port=$mysql_port", $mysql_username, $mysql_password);
    return $db;
} catch(PDOException $e) {
    print json_encode($e);
    http_send_status(500);
    die();
}

