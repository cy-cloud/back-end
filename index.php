<?php 
require __DIR__ . "/vendor/autoload.php";

$db = require __DIR__ . "/db.php";

require __DIR__ . "/models/Products.php";
require __DIR__ . "/models/Users.php";
$productsModel = new ProductsModel($db);
$userModel = new UsersModel($db);

$app = new Leaf\App;

$app->get('/products/{category}', function($category) use ($app) {
  $products = $productsModel->getByCategory($category);
  $app->response()->json($products);
});

$app->get('/{user_id}/cart', function() use ($app) {
  $app->response()->json(
    $userModel->getCart()
  );
});

$app->post('/{user_id}/cart', function($user_id) use ($app) {
  $app->response->json(
    $userModel->addToCart($app->request()->body())
  );
});

$app->post('/login', function() use ($app) {
  $username = $app->request()->get('username');
  $password = $app->request()->get('password');
  $app->response()->json(
    $userModel->login($username, $password)
  );
});

$app->post('/register', function() use ($app) {
  $app->response()->json(
    $userModel->register(
      $app->request()->body()
    )
  );
});

$app->run();