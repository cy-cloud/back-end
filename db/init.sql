DROP DATABASE cycloud IF EXISTS;
TRUNCATE Products, Users, Carts, ProductsCart;
DROP TABLE Products, Users, Carts, ProductsCart IF EXISTS;


CREATE DATABASE cycloud ;

USE cycloud;

CREATE TABLE Products (
    _id VARCHAR(36) PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    category ENUM('general', 'cpu', 'memory') NOT NULL,
    price DECIMAL(5, 2) NOT NULL CHECK(price >= 0),
);

CREATE TABLE Users  (
    _id VARCHAR(36) PRIMARY KEY NOT NULL,
    username TEXT NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL
);

CREATE TABLE Carts  (
    _id VARCHAR(36) PRIMARY KEY NOT NULL,
    user_id VARCHAR(36) FOREIGN KEY REFERENCES Users(_id),
);

CREATE TABLE ProductsCart (
    cart_id FOREIGN KEY REFERENCES Carts(_id),
    product_id FOREIGN KEY REFERENCES Products(_id),
    PRIMARY KEY (cart_id, product_id),
);