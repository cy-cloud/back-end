<?php 

class ProductsModel {
    private PDO $db;

    function __construct(PDO $db) {
        $this->$db = $db;
    }

    function getByCategory($category) {
        $sql = 'SELECT _id, name, category, price FROM Products WHERE category = :category';
        $query = $this->$db->prepare($sql);
        $query->bindParam(':category', $category);
        $result = $query->execute();
        return $result->fetchAll();
    }
}