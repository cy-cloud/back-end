<?php

use Leaf\Auth;

class Users {
    private PDO $db;

    function __construct(PDO $db) {
        $this->$db = $db;
    }

    function getUser() {
        return Auth::user("Users", ['password']);
    }

    function getCart() {
        $user = $this->getUser();
        $sql = 'SELECT p._id, p.name, p.category, p.price FROM Products p JOIN Cart c ON c.user_id = :id JOIN ProductsCart pc ON pc.cart_id = c._id WHERE p._id = pc.product_id';
        $query = $this->$db->prepare($sql);
        $query->bindParam(':id', $user['_id']);
        $result = $query->execute();
        return $result->fetchAll();
    }

    function login($username, $password) {
        return Auth::login('Users', [
            "username" => $username,
            "password" => md5($password)
        ]);
    }

    function register($payload) {
        [$username, $password, $fields] = $payload;
        return Auth::register("Users", [
            "username" => $username,
            "email" => $password,
            $fields
          ]);
    }

    function addToCart($product) {
        $user = $this->getUser();
        $sql = 'SELECT Cart._id FROM Carts WHERE Cart.user_id = :id';
        $query = $this->$db->prepare($sql);
        $query->bindParam(':id', $user['_id']);
        $result = $query->execute();
        $cart = $result->fetchAll()[0];
        $sql = 'INSERT INTO ProductsCart (cart_id, product_id) VALUES (:cartId, :productId)';
        $query = $this->$db->prepare($sql);
        $query->bindParam(':cartId', $cart['_id']);
        $query->bindParam(':productId', $product['_id']);
        $query->execute();
        return $this->getCart();
    }
}